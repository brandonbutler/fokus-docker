# fokus-docker

This is just a repo that clones the [official Fokus repo](https://github.com/pg07codes/fokus) and builds a docker image available in this project's image registry.

Take a look at the image registry here: https://gitlab.com/brandonbutler/fokus-docker/container_registry

## Use the image

```
docker run -d --name fokus -p 5000:5000 registry.gitlab.com/brandonbutler/fokus-docker:latest
```
